# Lab 4: Using the ESP32
## IWKS 5120 The Internet of Things
### John DiBaggio
March 2, 2017

### Included:
- Exercise 1: Blink an LED
- Exercise 1/2: SPDT Switch Input with 2 LEDs
- Exercise 1/2: 3 Push-button Switch Input Pullups with a Common-Anode RGB LED
- Exercise 2/3/4/5: 3 Push-button Switch Input Pullups with a Common-Anode RGB LED - uses a photocell to increase the brightness of the LEDs when more ambient light is detected and decrease the LED brightness when less ambient light is detected
- Exercise 3/4/5: Adjust the brightness of an LED based on how much force is put on a pressure resistor sensor
- Exercise 4: Pulse Width Modulated digital output with a Common-Anode RGB LED
- Exercies 2/6: Temperature from sensor (RHT03) and from the Internet (api.openweathermap.org)
- Exercise 6: Basic readings from the RHT03 Temperature & Humidity Sensor
- Exercise 6: Basic readings from the HC-SR04 Ultrasonic Ping Sensor

Each exercise sketch name is prefixed with "ex" followed by the number(s) of the category(ies) it includes.

All of the exercises contain the Arduino sketch as well as photos demonstrating the exercise. Some of the exercises contain a schematic and/or breadboard diagram built with Fritzing.
