/**
 * Exercise 4: Pulse Width Modulated digital output
 * Uses Sigma Delta modulation to gradually switch between different colors of an RGB LED
 * John DiBaggio
 * 
 */

const int PIN_LED_R =     16;
const int PIN_LED_G =     17;
const int PIN_LED_B =     4;
const int CHANNEL_R =     0;
const int CHANNEL_G =     1;
const int CHANNEL_B =     2;

const int DELAY = 10;

int brightnessR = 255;
int brightnessG = 0;
int brightnessB = 0;

void setup()
{
    // set up channels with frequency 312500 Hz
    sigmaDeltaSetup(CHANNEL_R, 312500);
    sigmaDeltaSetup(CHANNEL_G, 312500);
    sigmaDeltaSetup(CHANNEL_B, 312500);

    // attach LED pins to channels
    sigmaDeltaAttachPin(PIN_LED_R, CHANNEL_R);
    sigmaDeltaAttachPin(PIN_LED_G, CHANNEL_G);
    sigmaDeltaAttachPin(PIN_LED_B, CHANNEL_B);

    // initialize channels to off
    sigmaDeltaWrite(CHANNEL_R, 0);
    sigmaDeltaWrite(CHANNEL_G, 0);
    sigmaDeltaWrite(CHANNEL_B, 0);
}

void loop()
{
    for (int i = 0; i < 255; ++i) {
        --brightnessR;
        ++brightnessG;
        sigmaDeltaWrite(CHANNEL_R, 255 - brightnessR);
        sigmaDeltaWrite(CHANNEL_G, 255 - brightnessG);
        delay(DELAY);
    }

    for (int i = 0; i < 255; ++i) {
        --brightnessG;
        ++brightnessB;
        sigmaDeltaWrite(CHANNEL_G, 255 - brightnessG);
        sigmaDeltaWrite(CHANNEL_B, 255 - brightnessB);
        delay(DELAY);
    }

    for (int i = 0; i < 255; ++i) {
        --brightnessB;
        ++brightnessR;
        sigmaDeltaWrite(CHANNEL_B, 255 - brightnessB);
        sigmaDeltaWrite(CHANNEL_R, 255 - brightnessR);
        delay(DELAY);
    }

    for (int i = 0; i < 255; ++i) {
        --brightnessR;
        ++brightnessB;
        sigmaDeltaWrite(CHANNEL_R, 255 - brightnessR);
        sigmaDeltaWrite(CHANNEL_B, 255 - brightnessB);
        delay(DELAY);
    }

    for (int i = 0; i < 255; ++i) {
        --brightnessB;
        ++brightnessG;
        sigmaDeltaWrite(CHANNEL_B, 255 - brightnessB);
        sigmaDeltaWrite(CHANNEL_G, 255 - brightnessG);
        delay(DELAY);
    }

    for (int i = 0; i < 255; ++i) {
        --brightnessG;
        ++brightnessR;
        sigmaDeltaWrite(CHANNEL_G, 255 - brightnessG);
        sigmaDeltaWrite(CHANNEL_R, 255 - brightnessR);
        delay(DELAY);
    }
}
