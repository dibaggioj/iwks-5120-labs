/**
 * Exercise 3/4/5
 * Adjust the brightness of an LED based on how much force is put on a pressure resistor sensor
 * John DiBaggio
 * February 23, 2017
 */

const int PIN_INPUT_FORCE = 4;
const int PIN_OUTPUT_LED = 17;
const int PIN_CHANNEL_LED = 0;

void setup()
{
    pinMode(PIN_INPUT_FORCE, INPUT);
    pinMode(PIN_OUTPUT_LED, OUTPUT);
    
    sigmaDeltaSetup(PIN_CHANNEL_LED, 312500);             // setup channel PIN_CHANNEL_LED with frequency 312500 Hz
    sigmaDeltaAttachPin(PIN_OUTPUT_LED, PIN_CHANNEL_LED); // attach pin PIN_OUTOUT_LED to channel 0    
    sigmaDeltaWrite(PIN_CHANNEL_LED, 0);                  // initialize channel PIN_CHANNEL_LED to off
    
    Serial.begin(115200);
    Serial.println("Awaiting pressure to turn on light...");
}

void loop()
{
    int forceReading = analogRead(PIN_INPUT_FORCE);        
    int ledBrightness = map(forceReading, 0, 3000, 0, 255);
    
    Serial.print("Analog reading: ");
    Serial.print(forceReading);
    Serial.print(", LED brightness: ");
    Serial.println(ledBrightness);
    
    sigmaDeltaWrite(PIN_CHANNEL_LED, ledBrightness);
    
    delay(100);
}

