/**
 * Exercise 1: Blink an LED
 * John DiBaggio
 * February 23, 2017
 * 
 * Modified from John Bennett
 * Make an external LED blink
 * 
 */

const int LED_PIN = 17; 

void setup()
{
    pinMode(LED_PIN, OUTPUT);
    Serial.begin(115200);
    Serial.println("Testing LED...");
}

void loop()
{
    digitalWrite(LED_PIN, HIGH);
    delay(500);
    digitalWrite(LED_PIN, LOW);
    delay(750);
}

