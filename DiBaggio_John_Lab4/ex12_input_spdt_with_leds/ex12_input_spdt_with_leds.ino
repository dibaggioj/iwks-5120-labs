/**
 * Exercise 2
 * SPDT Switch Input with 2 LEDs
 * John DiBaggio
 * February 25, 2017
 * 
 */


const int PIN_SPDT_SWITCH = 25;
const int PIN_LED_1 =       16;
const int PIN_LED_2 =       17;


void setup() {
    pinMode(PIN_SPDT_SWITCH, INPUT);
    pinMode(PIN_LED_1, OUTPUT);
    pinMode(PIN_LED_2, OUTPUT);
}

void loop() {
    int switchVal = digitalRead(PIN_SPDT_SWITCH);
    if (switchVal == HIGH) {
        digitalWrite(PIN_LED_1, LOW);
        digitalWrite(PIN_LED_2, HIGH);
    } else {
        digitalWrite(PIN_LED_2, LOW);
        digitalWrite(PIN_LED_1, HIGH);
    }
}
