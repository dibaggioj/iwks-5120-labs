/**
 * Exercise 6: Temperature & Humidity Sensor (RHT03)
 * John DiBaggio
 * February 23, 2017
 * **************************************************************************
 * Modified from the following:
 *
 * Jim Lindblom <jim@sparkfun.com>
 * August 31, 2015
 *   
 * Ported to Arduino by Shawn Hymel
 * October 28, 2016
 * https://github.com/sparkfun/SparkFun_RHT03_Arduino_Library
 *
 * Modified for ESP32 by John Bennett
 * February 7, 2017
 * 
 * This a simple example sketch that uses the SparkFunRHT03 Ardiuno
 * library.
 * 
 * Looking at the front (grated side) of the RHT03 (DHT22), the pinout is as follows:
 *  1     2        3       4
 * VCC  DATA  No-Connect  GND
 *
 * Connect VCC of the RHT03 to 3.3V
 * Connect the data pin to ESP32 pin 4 (or the pin of your choice)
 * Connect GND to GND
 * 
 * It is not necessary to add a pullup resistor to the data pin; the library code uses the internal pullup.
 * 
 * Development environment specifics:
 * Arduino IDE v1.8.1; ESP32 Arduino pull of Feb. 7, 2017
 * Distributed as-is; no warranty is given.  
*/

#include <SparkFun_RHT03.h>

const int RHT03_DATA_PIN = 4; // RHT03 data pin

RHT03 rht; // Creates an RTH03 object to interact with the sensor

void setup()
{
	Serial.begin(115200); // Serial is used to print sensor readings.
	rht.begin(RHT03_DATA_PIN);  // initialize the sensor and our data pin
	Serial.println("Measuring humidity and temperature...");
}

void loop() {
	// Call rht.update() to get new humidity and temperature values from the sensor.
	int updateRet = rht.update();
	
	// If successful, the update() function will return 1. If update fails, it will return a value <0
	if (updateRet == 1) {
		// The humidity(), tempC(), and tempF() functions can be called -- after 
		// a successful update() -- to get the last humidity and temperature value
		float latestHumidity = rht.humidity();
		float latestTempC = rht.tempC();
		float latestTempF = rht.tempF();
		
		Serial.print("Humidity: " + String(latestHumidity, 1) + "%, ");
		Serial.print("Temperature: " + String(latestTempF, 1) + "\u00B0F, ");
		Serial.println(String(latestTempC, 1) + "\u00B0C");
	} else {
		// If the update failed, try delaying for RHT_READ_INTERVAL_MS ms before trying again.
    Serial.println("Failed");
		delay(RHT_READ_INTERVAL_MS);
	}
	
	delay(1000);
}
