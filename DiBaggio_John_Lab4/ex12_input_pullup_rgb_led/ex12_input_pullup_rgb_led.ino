/**
 * Exercise 2
 * 3 Push-button Switch Input Pullups with a Common-Anode RGB LED
 * Uses digital writes to light up the LED
 * John DiBaggio
 * February 25, 2017
 * 
 */

const int PIN_SWITCH_R =  12;
const int PIN_SWITCH_G =  13;
const int PIN_SWITCH_B =  14;
const int PIN_LED_R =     16;
const int PIN_LED_G =     17;
const int PIN_LED_B =     4;

void setup()
{
    pinMode(PIN_SWITCH_R, INPUT_PULLUP);
    pinMode(PIN_SWITCH_G, INPUT_PULLUP);
    pinMode(PIN_SWITCH_B, INPUT_PULLUP);

    pinMode(PIN_LED_R, OUTPUT);
    pinMode(PIN_LED_G, OUTPUT);
    pinMode(PIN_LED_B, OUTPUT);
}

void loop()
{
    // Read 0 when pushed, 1 when not pushed
    int switchRPushed = !digitalRead(PIN_SWITCH_R);
    int switchGPushed = !digitalRead(PIN_SWITCH_G);
    int switchBPushed = !digitalRead(PIN_SWITCH_B);

    // Invert values since a common-anode LED is used
    digitalWrite(PIN_LED_R, !switchRPushed);
    digitalWrite(PIN_LED_G, !switchGPushed);
    digitalWrite(PIN_LED_B, !switchBPushed);
    
    delay(100);
}
