/**
 * Exercise 2/6
 * Temperature from a sensor and from the Internet
 * RHT03 Sensors and api.openweathermap.org API
 * John DiBaggio
 * February 23, 2017
 * 
 * Connects to a Wifi network.
 * Each time the button is pressed:
 *  - Gets and prints out the inside temperature using the RHT03 temperature & humidity sensor.
 *  - Gets and prints out the outside temperature from api.openweathermap.org
 *  - Prints out the temperature difference between inside and outside
 * 
 * For example:
 *     Connecting to WiFi network: <some network name>
 *     ........
 *     WiFi connected!
 *     IP address: 192.168.0.106
 *     Press button to get temperatures.
 *     Acquiring temperatures...
 *
 *     ------------------------------
 *     Looking up current outside temperature...
 *     Connecting to domain: api.openweathermap.org
 *     Connected!
 *
 *     ------------------------------
 *
 *     Closing connection
 *
 *     It is currently -3.2°C (26.2°F) outside.
 *     It is currently 20.3°C (68.5°F) inside.
 *     It is 23.5°C (42.3°F) colder outside.
 * 
 * Uses libraries:
 *  - Sparkfun RHT03 https://github.com/sparkfun/SparkFun_RHT03_Arduino_Library
 *  - ArduinoJson https://github.com/bblanchon/ArduinoJson/
 *  - WiFi in the Arduino Core for ESP32 https://github.com/espressif/arduino-esp32/tree/master/libraries/WiFi
 * 
 * Includes some code modified from:
 *  - https://learn.sparkfun.com/tutorials/esp32-thing-hookup-guide#arduino-example-wifi
 *  - Jim Lindblom
 *  - John Bennett
 */

#include "ArduinoJson.h"
#include "Conf.h"
#include "SparkFun_RHT03.h"
#include "WiFi.h"

const int BUTTON_PIN = 25;
const int RHT03_DATA_PIN = 4;

// WiFi network name and password:
const char *networkName;
const char *networkPswd;

// Weather app:
const char *HOST_DOMAIN = "api.openweathermap.org";
const uint8_t HOST_PORT = 80;
const char *DENVER_LAT = "39.7392";
const char *DENVER_LNG = "-104.9903";
const float T_ABS_ZERO_C = -273.15;
const char *hostAppId;


RHT03 rht;

void setup() {
    networkName = Conf::ENV_NETWORK_NAME;
    networkPswd = Conf::ENV_NETWORK_PSWD;
    hostAppId = Conf::ENV_HOST_APP_ID;
  
    // Initialize hardware
    Serial.begin(115200);
    pinMode(BUTTON_PIN, INPUT_PULLUP);
    pinMode(BUILTIN_LED, OUTPUT);
    rht.begin(RHT03_DATA_PIN);  // initialize the sensor and data pin

    // Connect to the WiFi network
    connectToWiFi(networkName, networkPswd);

    digitalWrite(BUILTIN_LED, LOW);
    Serial.println("Press button to get temperatures.");
}

void loop() {
    if (digitalRead(BUTTON_PIN) == LOW) { // Check if button has been pressed
        while (digitalRead(BUTTON_PIN) == LOW) {
            ; // Wait for button to be released
        }
        digitalWrite(BUILTIN_LED, HIGH);  // Turn on LED
        compareTemps();
        digitalWrite(BUILTIN_LED, LOW);   // Turn off LED
    }
}

void connectToWiFi(const char *ssid, const char *pwd) {
    int ledState = 0;

    printLine();
    Serial.println("Connecting to WiFi network: " + String(ssid));

    WiFi.begin(ssid, pwd);

    while (WiFi.status() != WL_CONNECTED) {
        // Blink LED while we're connecting:
        digitalWrite(BUILTIN_LED, ledState);
        ledState = (ledState + 1) % 2; // Flip ledState
        delay(500);
        Serial.print(".");
    }
    
    Serial.println();
    Serial.println("WiFi connected!");
    Serial.print("IP address: ");
    Serial.println(WiFi.localIP());
}

/**
 * Gives an output like:
 * It is currently 16.8°C, 30.2°F colder outside.
 */ 
void compareTemps() {
    Serial.println("Acquiring temperatures...");
    float tempOutsideC = getTempInternet();
    float tempOutsideF = 1.8 * tempOutsideC + 32;
    float tempInsideC = getTempSensor();
    float tempInsideF = 1.8 * tempInsideC + 32;

    if (tempOutsideC <= T_ABS_ZERO_C || tempInsideC <= T_ABS_ZERO_C) {
        if (tempOutsideC <= T_ABS_ZERO_C) {
            Serial.println("There was an error acquiring the outside temperature.");
        } else {
            Serial.println("It is currently " + String(tempOutsideC, 1) + "\u00B0C (" + String(tempOutsideF, 1) + "\u00B0F) outside.");
        }

        if (tempInsideC <= T_ABS_ZERO_C) {
            Serial.println("There was an error acquiring the inside temperature.");
        } else {
            Serial.println("It is currently " + String(tempInsideC, 1) + "\u00B0C (" + String(tempInsideF, 1) + "\u00B0F) inside.");
        }
            
        return;
    }

    Serial.println("It is currently " + String(tempOutsideC, 1) + "\u00B0C (" + String(tempOutsideF, 1) + "\u00B0F) outside.");
    Serial.println("It is currently " + String(tempInsideC, 1) + "\u00B0C (" + String(tempInsideF, 1) + "\u00B0F) inside.");

    float tempDeltaC = tempOutsideC - tempInsideC;
    float tempDeltaF = tempDeltaC * 1.8;

    if (tempDeltaC >= 0) {
        Serial.println("It is " + String(tempDeltaC, 1) + "\u00B0C (" + String(tempDeltaF, 1) + "\u00B0F) warmer outside.");
    } else {
        Serial.println("It is " + String(tempDeltaC * -1, 1) + "\u00B0C (" + String(tempDeltaF * -1, 1) + "\u00B0F) colder outside.");
    }
}

inline float getTempSensor() {
    return getTempSensor(0);
}

float getTempSensor(int attempt) {
    float temp = T_ABS_ZERO_C;
    if (attempt > 2) {
        return temp;
    }
    
    int updateRet = rht.update();
    if (updateRet == 1) {     // If successful, the update() will return 1. If update fails, it will return a value <0
        temp = rht.tempC();   // Get temperature after a successful update
    } else {
        // If the update failed, try delaying for RHT_READ_INTERVAL_MS ms before trying again.
        Serial.println("Failed");
        delay(RHT_READ_INTERVAL_MS);
        ++attempt;
        temp = getTempSensor(attempt);
    }

    return temp;
}

float getTempInternet() {
    float temp = T_ABS_ZERO_C;
    WiFiClient client;  // Use WiFiClient class to create TCP connections
    
    printLine();
    Serial.println("Looking up current outside temperature...");
    Serial.print("Connecting to domain: ");
    Serial.println(HOST_DOMAIN);

    if (!client.connect(HOST_DOMAIN, HOST_PORT)) {
        Serial.println("Connection failed");
        return temp;
    }
    Serial.println("Connected!");
    printLine();

    // This will send the request to the server
    client.print((String)"GET /data/2.5/weather?lat=" + DENVER_LAT + "&lon=" + DENVER_LNG + "&APPID=" + hostAppId + " HTTP/1.1\r\n" +
                             "Host: " + HOST_PORT + "\r\n" +
                             "Connection: close\r\n\r\n");
                             
    unsigned long timeout = millis();
    while (client.available() == 0) {
        if (millis() - timeout > 5000) {
            Serial.println(">>> Client Timeout !");
            client.stop();
            return temp;
        }
    }
    
    String response = "";
    while (client.available()) {  // Read all the lines of the reply from server
        response = client.readStringUntil('\r');
    }

    if (response) {
        const int jsonIndex = response.indexOf('{');
        response = response.substring(jsonIndex);
        const int responseLength = response.length() + 1; // accomodate for end-of-line character
        char* responseBody = new char[responseLength];
        response.toCharArray(responseBody, responseLength);

        temp = parseTemp(responseBody);
    } else {
        Serial.println("No response received");
    }

    Serial.println("Closing connection");
    Serial.println();
    client.stop();

    return temp - 273.15;
}

float parseTemp(const char *responseBody) {
    float temp = T_ABS_ZERO_C;
    // Calculated size at https://bblanchon.github.io/ArduinoJson/assistant/
    const size_t bufferSize = JSON_ARRAY_SIZE(1) + 2 * JSON_OBJECT_SIZE(1) + JSON_OBJECT_SIZE(2) + 
        JSON_OBJECT_SIZE(4) + JSON_OBJECT_SIZE(5) + JSON_OBJECT_SIZE(6) + JSON_OBJECT_SIZE(12) + 950;
    DynamicJsonBuffer jsonBuffer(bufferSize);
    
    JsonObject& root = jsonBuffer.parseObject(responseBody);
    if (!root.success()) {
        Serial.println("JSON object parse failed");
        return temp;
    }
    if (!root.containsKey("main") || !root["main"].is<JsonObject&>()) {
        Serial.println("No 'main' object in JSON response object.");
        return temp;
    }
    
    JsonObject& main = root["main"];  // root["main"].as<JsonObject>();
    temp = main["temp"];
    if (!temp) {
        return T_ABS_ZERO_C;
    }

    return temp;
}

void printLine() {
    Serial.println();
    for (int i=0; i<30; i++) {
        Serial.print("-");
    }
    Serial.println();
}

