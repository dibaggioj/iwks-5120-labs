/**
 * Exercise 6: Ultrasonic Ping Sensor
 * John DiBaggio
 * February 28, 2017
 *  
 * Light up internal LED of ESP32 when calculated distance based on ping response time is out of range.
 * Light up external LED with varying brightness based on the distance of object from sensor (brighter 
 * when closer, dimmer when farther), and turn it off when object is out of range.
 *  
 * HC-SR04 connected to ESP32 via NCP1402 step-up converter and BOB-12009 bidirectional logic-level converter
 * Note, this uses the internal pullup resistor of the HC-SR04
 *
 * This sketch originates from Virtualmix: http://goo.gl/kJ8Gl
 * Has been modified by Winkle ink here: http://winkleink.blogspot.com.au/2012/05/arduino-hc-sr04-ultrasonic-distance.html
 * And modified further by ScottC here: http://arduinobasics.blogspot.com.au/2012/11/arduinobasics-hc-sr04-ultrasonic-sensor.html
 * on 10 Nov 2012.
 * And modified again by JKB in Feb 2017
 * And modified again by John DiBaggio in Feb 2017
 */
 
const int ECHO_PIN = 12;    // Echo Pin
const int TRIG_PIN = 14;    // Trigger Pin
const int MAX_RANGE = 200;  // Maximum range needed
const int MIN_RANGE = 0;    // Minimum range needed
const int LED_PIN = 13;
const int LED_CHANNEL = 0;

long duration, distance;    // Duration used to calculate distance
long brightness = 0;

void setup() {
  Serial.begin(115200);
  Serial.println("Recording distances...");
  
  pinMode(TRIG_PIN, OUTPUT);
  pinMode(ECHO_PIN, INPUT);

  // Use LED OUT-OF-RANGE indicator
  pinMode(BUILTIN_LED, OUTPUT);
  
  // Use LED distance indicator
  sigmaDeltaSetup(LED_CHANNEL, 312500);
  sigmaDeltaAttachPin(LED_PIN, LED_CHANNEL);
  sigmaDeltaWrite(LED_CHANNEL, brightness);
}

void loop() {
  /**
   * The following TRIG_PIN/ECHO_PIN cycle is used to determine the distance of the nearest
   * object by bouncing soundwaves off of it
   */
  digitalWrite(TRIG_PIN, LOW); 
  delayMicroseconds(2); 
  
  digitalWrite(TRIG_PIN, HIGH);
  delayMicroseconds(10); 
  
  digitalWrite(TRIG_PIN, LOW);
  duration = pulseIn(ECHO_PIN, HIGH); // pulseIn required Feb 2017 ESP32 code

  // Calculate the distance (in cm) based on the speed of sound.
  distance = duration / 58.2;
 
  if (distance >= MAX_RANGE || distance <= MIN_RANGE) {
    /** 
     * Send out-of-range message to the computer using Serial protocol.
     * Turn internal LED ON to indicate out of range
     * Turn of external LED since there is no valid distance to display
     */
    digitalWrite(BUILTIN_LED, HIGH);
    sigmaDeltaWrite(LED_CHANNEL, brightness);
    Serial.println("Out of Range");
  } else {
    /** 
     * Send the distance to the computer using Serial protocol.
     * Turn internal LED OFF to indicate successful reading.
     * Light up external LED based on distance: brighter when smaller, dimmer when larger
     */
    digitalWrite(BUILTIN_LED, LOW);
    brightness = map(distance, MIN_RANGE, MAX_RANGE, 255, 127);
    sigmaDeltaWrite(LED_CHANNEL, brightness);
    Serial.print(distance);
    Serial.println("cm");
  }
 
  // Delay 50ms before next reading.
  delay(50);
}
