/**
 * Exercise 5 (exercies 2, 3, and 4 combined)
 * 3 Push-button Switch Input Pullups with a Common-Anode RGB LED
 * Uses a photocell to increase the brightness of the LEDs when more ambient light is detected and
 * decrease the LED brightness when less ambient light is detected
 * John DiBaggio
 * February 25, 2017
 * 
 */

const int PIN_PHOTOCELL = 15;
const int PIN_SWITCH_R =  12;
const int PIN_SWITCH_G =  13;
const int PIN_SWITCH_B =  14;
const int PIN_LED_R =     16;
const int PIN_LED_G =     17;
const int PIN_LED_B =     4;
const int CHANNEL_R =     0;
const int CHANNEL_G =     1;
const int CHANNEL_B =     2;

void setup()
{
    pinMode(PIN_PHOTOCELL, INPUT);
    pinMode(PIN_SWITCH_R, INPUT_PULLUP);
    pinMode(PIN_SWITCH_G, INPUT_PULLUP);
    pinMode(PIN_SWITCH_B, INPUT_PULLUP);

    // set up channels with frequency 312500 Hz
    sigmaDeltaSetup(CHANNEL_R, 312500);
    sigmaDeltaSetup(CHANNEL_G, 312500);
    sigmaDeltaSetup(CHANNEL_B, 312500);

    // attach LED pins to channels
    sigmaDeltaAttachPin(PIN_LED_R, CHANNEL_R);
    sigmaDeltaAttachPin(PIN_LED_G, CHANNEL_G);
    sigmaDeltaAttachPin(PIN_LED_B, CHANNEL_B);

    // initialize channels to off
    sigmaDeltaWrite(CHANNEL_R, 0);
    sigmaDeltaWrite(CHANNEL_G, 0);
    sigmaDeltaWrite(CHANNEL_B, 0);
}

void loop()
{
    int lightReading = analogRead(PIN_PHOTOCELL);
    // invert the mapping, so LEDs are brigter when more light is detected ( higher resistance)
    int brightnessLED = map(lightReading, 0, 4095, 255, 0);

    // Read 0 when pushed, 1 when not pushed
    int switchRPushed = !digitalRead(PIN_SWITCH_R);
    int switchGPushed = !digitalRead(PIN_SWITCH_G);
    int switchBPushed = !digitalRead(PIN_SWITCH_B);

    int brightnessR = switchRPushed * brightnessLED;
    int brightnessG = switchGPushed * brightnessLED;
    int brightnessB = switchBPushed * brightnessLED;

    // Substract brightness vals from 255 since a common-anode LED is used
    sigmaDeltaWrite(CHANNEL_R, 255 - brightnessR);
    sigmaDeltaWrite(CHANNEL_G, 255 - brightnessG);
    sigmaDeltaWrite(CHANNEL_B, 255 - brightnessB);
    
    delay(100);
}
