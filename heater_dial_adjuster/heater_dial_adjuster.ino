/**
 * Hot Water Heater Dial
 * Servo model EMAX ES08A11
 * March 8, 2017
 * Start with SPDT switch turned to "off" (towards GND), and with motor detached from dial. 
 * After starting the sketch, attach the motor to the dial with the motor's wires
 * facing to the left (toward the area between "HOT" and "VACATION")
 */

#include <Servo.h>

const int PIN_SERVO = 10;
const int PIN_SPDT = 11;
const int PIN_LOW_LIMIT = 2;

int minServoPosition = 169;
int maxServoPosition = 170;
int servoPosition = 0;
bool on = false;
bool lowSet = false;
bool highSet = false;

Servo servo;

void setup() {
  Serial.begin(9600);

  initHardware();
  
  Serial.println("Awaiting dial calibration..");
  
  calibrate();

  Serial.println("Awaiting dial adjustment..");
}

void loop() {
  int switchVal = digitalRead(PIN_SPDT);
  if (switchVal == HIGH) {
      turnOn();
  } else {
      turnOff();
  }
  
  delay(100);
}

void initHardware() {
  pinMode(PIN_SPDT, INPUT);
  pinMode(PIN_LOW_LIMIT, INPUT_PULLUP);
  
  servo.attach(PIN_SERVO);
}

void calibrate() {
  Serial.println("Max: " + String(maxServoPosition) + "\u00B0");
  
  turnOn();

  Serial.println("Attach motor to dial now.");

  bool isSetting = false;

  Serial.println("Set lower limit now.");
  
  while (!lowSet) {
    if (!digitalRead(PIN_LOW_LIMIT)) {
      isSetting = true;
      if (minServoPosition > 0) {
        minServoPosition -= 2;
        Serial.println(minServoPosition);
        servo.write(minServoPosition);
      } else {
        lowSet = true;
        isSetting = false;
      }
    } else if (isSetting) {
      lowSet = true;
      isSetting = false;
    }

    delay(200);
  }
  
  Serial.println("Min: " + String(minServoPosition) + "\u00B0");

  servoPosition = minServoPosition;
  
  on = false;
}

void turnOn() {
  if (!on) {
    Serial.println("On");
    on = true;
    for (; servoPosition < maxServoPosition; servoPosition += 1) {
      Serial.print(servoPosition);
      Serial.print(", ");
      servo.write(servoPosition);
      delay(10);
    }
    Serial.println();
  }
}

void turnOff() {
  if (on) {
    Serial.println("Off");
    on = false;
    for (; servoPosition > minServoPosition; servoPosition -= 1) {
      Serial.print(servoPosition);
      Serial.print(", ");
      servo.write(servoPosition);
      delay(10);
    }
    Serial.println();
  }
}

