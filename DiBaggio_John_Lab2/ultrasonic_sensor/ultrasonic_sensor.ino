/**
 * 
 * Ultrasonic Sensor
 * 
 * John DiBaggio
 * Kyle Etsler
 * Laura Meckley
 * 
 * Code to demonstrate the functioning of the Ultrasonic sensor, model HC-SR04
 * 
 */
 
const int trig_pin = 12;
const int echo_pin = 11;
long duration, distance;

void setup() {
  pinMode(trig_pin, OUTPUT);
  pinMode(echo_pin, INPUT);
  Serial.begin(9600);
}

void loop() {
  digitalWrite(trig_pin, LOW);
  delayMicroseconds(2);

  // Sets the trig pin on HIGH state for 10 micro seconds
  digitalWrite(trig_pin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trig_pin, LOW);

  duration = pulseIn(echo_pin, HIGH);

  // Calculating the distance in CM
  distance = duration * 0.034 / 2;
  Serial.println(distance);
  
  delay(500);
}
