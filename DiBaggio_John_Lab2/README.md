# Lab 2: Arduino 2 - Arduino with sensors and actuators
## IWKS 5120 The Internet of Things
### John DiBaggio

#### Collaborated with Kyle Etsler and Laura Meckley for some of the sensors

#### For each sensor or actuator, included are code demoing the use of the device with an Arduino board and a photo showing it connected or in use. Some also include a diagram built with Fritzing.

### Included:
- Photoresistor
- IR Emitter Receiver 
- Ultrasonic Sensor
- Pressure Sensor
- Flexible Resistor
- Temperature Sensor
- Tilt Sensor
- Pushbutton Switch
- Potentiometer
- Servo Motor
