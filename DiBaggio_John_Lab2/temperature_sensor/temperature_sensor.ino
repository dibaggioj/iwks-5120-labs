/**
 * 
 * Temperature Sensor (Thermistor)
 * 
 * John DiBaggio
 * 
 * modified from https://learn.adafruit.com/thermistor/using-a-thermistor
 * 
 * Vout = Rtherm / (Rtherm + Rres) * Vcc
 * ADCval = Vin * 1023 / Vcc
 * Vout = Vin
 * So,
 * ADCval = Rtherm / (Rtherm + Rres) * 1023
 * Rtherm = Rres * ADCval / (1023 - ADC)
 * 
 * The thermistor used is supposed to be an NTC thermistor, but it seems that resistance increases with
 * temperature, which suggests that it's a PTC thermistor.
 * 
 */

const int thermistor_pin = A0;
const int r_resistor = 10000;

void setup(void) {
  Serial.begin(9600);
  pinMode(thermistor_pin, INPUT);
}
 
void loop(void) {
  float reading = analogRead(thermistor_pin);
 
  Serial.print("Analog reading: "); 
  Serial.println(reading);
  
  float r_thermistor = reading * r_resistor / (1023 - reading); // convert the value to resistance
 
  Serial.print("Thermistor resistance: "); 
  Serial.println(r_thermistor);
 
  delay(1000);
}
