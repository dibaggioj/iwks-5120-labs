/**
 * 
 * Ultrasonic Sensor
 * 
 * John DiBaggio
 * Kyle Etsler
 * Laura Meckley
 * 
 * Code to demonstrate the functioning of the felix sensor
 * 
 * Modified from Jim Lindblom https://www.sparkfun.com/products/10264
 * Create a voltage divider circuit combining a flex sensor with a 47k resistor.
 * The resistor should connect from A0 to GND.
 * The flex sensor should connect from A0 to 3.3V
 * As the resistance of the flex sensor increases (meaning it's being bent), the voltage at A0 should decrease.
 * 
 */
 
const int FLEX_PIN = A0;
const float R_RESISTOR = 1000.0;

// Upload the code, then try to adjust these values to more accurately calculate bend degree.
const float STRAIGHT_RESISTANCE = 37300.0;  // resistance when straight
const float BEND_RESISTANCE = 90000.0;      // resistance at 90 deg

void setup() {
  Serial.begin(9600);
  pinMode(FLEX_PIN, INPUT);
}

void loop() {
  // Read the ADC, and calculate voltage and resistance from it
  int adc_flex = analogRead(FLEX_PIN);
  float r_flex = R_RESISTOR * (1023 / adc_flex - 1);
  Serial.println("Resistance: " + String(r_flex) + " ohms");

  // Use the calculated resistance to estimate the sensor's bend angle:
  float angle = map(r_flex, STRAIGHT_RESISTANCE, BEND_RESISTANCE, 0, 90.0);
  Serial.println("Bend: " + String(angle) + " degrees");

  delay(500);
}

