/**
 * 
 * IR Emitter Receiver
 * 
 * John DiBaggio
 * Kyle Etsler
 * Laura Meckley
 * 
 * Code to demonstrate the functioning of the IR emitter receiver (proximity sensor), model TCRT1000.
 * The next step would be to calibrate the sensor by mapping the sensor values (range of 0-1023) to measured distances (up to 
 * a couple centimeters based off initial testing).
 * 
 */

int sensorIRPin = A0;
int ledIRPin =    10;
int sensorValue = 0;

void setup() {
  Serial.begin(9600);
  pinMode(ledIRPin, OUTPUT);
  pinMode(sensorIRPin, INPUT);
}

void loop() {
  digitalWrite(ledIRPin, HIGH);           // turn the IR LED on by making the voltage level HIGH
  delay(500);
  
  sensorValue = analogRead(sensorIRPin);  // read the analog value from IR sensor
  Serial.println(sensorValue);
  
  digitalWrite(ledIRPin, LOW);            // turn the IR LED off by making the voltage level LOW
  delay(500);
}
