/**
 *
 * Tilt Sensor Switch
 * 
 * John DiBaggio
 * 
 * Modified from https://learn.adafruit.com/tilt-sensor/using-a-tilt-sensor
 * 
 */
 
const int tilt_pin = 2;
const int led_pin = 10;
const long debounce = 50; // the debounce time, increase if the output flickers

int previous = LOW; // the previous reading from the input pin
long time = 0;      // the last time the output pin was toggled

void setup() {
  pinMode(tilt_pin, INPUT);
  pinMode(led_pin, OUTPUT);
}
 
void loop() {
  int reading = digitalRead(tilt_pin);
 
  // If the switch changed, due to bounce or pressing...
  if (reading != previous) {
    time = millis();  // Reset the debouncing timer
  } 
 
  if ((millis() - time) > debounce) {
    // Whatever the tilt switch is at, its been there for a long time, so settle on the reading value
    int tilt_state = reading;
    int led_state = (tilt_state == HIGH) ? LOW : HIGH; // Invert the output on the LED
    digitalWrite(led_pin, led_state);
  }
  
  // Save the last reading so we keep a running tally
  previous = reading;
}
