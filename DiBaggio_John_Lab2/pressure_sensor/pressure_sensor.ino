/**
 * 
 * Pressure Sensor (Force Sensor)
 * 
 * John DiBaggio
 * Kyle Etsler
 * Laura Meckley
 * 
 * Code to demonstrate the functioning of the Pressure sensor by lighting up an LED that gets brighter
 * the harder you press
 * 
 */
 
const int fsr_pin = A0;
const int led_pin = 10;

int fsr_reading;
int led_brightness;
 
void setup(void) {
  Serial.begin(9600);
  pinMode(led_pin, OUTPUT);
}
 
void loop(void) {
  fsr_reading = analogRead(fsr_pin);
  Serial.print("Analog reading = ");
  Serial.println(fsr_reading);
 
  // Map analog readin from 0-1023 range down to the range used by analogWrite (0-255)
  led_brightness = map(fsr_reading, 0, 1023, 0, 255);
  
  analogWrite(led_pin, led_brightness);
 
  delay(100);
}
