/**
 * 
 * Small servo motor
 * 
 * John DiBaggio
 * 
 * Servo model EMAX ES08A11
 * 
 * modified from http://www.arduino.cc/en/Tutorial/Sweep
 * 
 * Rotates the motor back and forth a different speeds adjusted in different ways.
 * 
 */

#include <Servo.h>
#include <math.h>

Servo servo;
int servo_pin = 10;
int servo_position = 0;

void setup() {
  Serial.begin(9600);
  servo.attach(servo_pin);  // attaches the servo on pin 10 to the servo object
}

void printPosition() {
  int scale = 10;
  if (servo.read() % scale == 0) {
    int value = servo.read() / scale;
    char *str = malloc(value + 1);
    memset(str, '-', value);
    str[value] = '\0';
    Serial.println(str);
    delete str;
  }
}

void loop() {
  
  /**
   * 1. Go from 0 to 180 degrees in steps of 1 degree. 
   * Wait 10 ms for the servo to reach the written position
   */
  for (servo_position = 0; servo_position <= 180; servo_position += 1) {
    servo.write(servo_position);
    printPosition();
    delay(10);
  }
  
  /**
   * 2. Go from 180 to 0 degrees in steps of 2 degrees but at slower rate (higher delay). 
   * Wait 20 ms for the servo to reach the written position.
   * Should be same rotational velocity as #1 above.
   */
  for (servo_position = 180; servo_position >= 0; servo_position -= 2) {
    servo.write(servo_position);
    printPosition();
    delay(20);
  }
  
  /**
   * 3. Go from 0 to 180 degrees in steps of 1 degree at faster rate (lower delay). 
   * Wait 5 ms for the servo to reach the written position.
   */
  for (servo_position = 0; servo_position <= 180; servo_position += 1) {
    servo.write(servo_position);
    printPosition();
    delay(5);
  }
  
  /**
   * 4. Go from 180 to 0 degrees in steps of 2 degrees at original rate (original delay)
   * Wait 10 ms for the servo to reach the written position.
   * Should be same rotational velocity as #3 above.
   */
  for (servo_position = 180; servo_position >= 0; servo_position -= 2) {
    servo.write(servo_position);
    printPosition();
    delay(10);
  }
}
