/**
 * 
 * Pushbutton Switch
 * 
 * John DiBaggio
 * 
 * modified from http://www.arduino.cc/en/Tutorial/InputPullupSerial
 *  
 * Lights up an external LED when the pushbutton switch is pressed.
 *  
 * Input Pullup Serial
 * Uses the internal 20K-ohm resistor, pulling it to 5V. This configuration causes the input to
 * read HIGH when the switch is open, and LOW when it is closed.
 * 
 */

int switchPin = 2;
int ledPin = 10;
int sensorVal = HIGH; // open input pullup

void setup() {
  Serial.begin(9600);
  Serial.println("Awaiting button push...");
  pinMode(switchPin, INPUT_PULLUP); // configure pin 2 as an input and enable the internal pull-up resistor
  pinMode(ledPin, OUTPUT);
}

void loop() {
  int sensorValLast = sensorVal;
  sensorVal = digitalRead(switchPin);
  if (sensorValLast != sensorVal) {
    Serial.println(sensorVal);
  }
  
  if (sensorVal == HIGH) {
    digitalWrite(ledPin, LOW);
  } else {
    digitalWrite(ledPin, HIGH);
  }
}
