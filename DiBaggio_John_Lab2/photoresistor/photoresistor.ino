/**
 * 
 * Photoresistor (Light Dependent Resistor)
 * 
 * John DiBaggio
 * 
 * Code to demonstrate the functioning of a photoresistor
 * 
 */

int sensorPin = A0;
int sensorValue = 0;

void setup() {
  Serial.begin(9600);
  pinMode(sensorPin, INPUT);
}

void loop() {
  sensorValue = analogRead(sensorPin);  // read the analog value from IR sensor
  Serial.println(sensorValue);
  delay(1000);
}
