/**
 * 
 * Variable Resistor (Potentiometer)
 * 
 * John DiBaggio
 * 
 * Potentiometer, model B10K
 * 
 * References:
 * http://www.instructables.com/id/How-to-use-Potentiometer-Arduino-Tutorial/
 * https://www.arduino.cc/en/Tutorial/Potentiometer
 * 
 */

const int pot_pin = A2;
const int led_pin = 10;

int sensor_value = 0;

void setup() {
  Serial.begin(9600);
  pinMode(led_pin, OUTPUT);
  pinMode(pot_pin, INPUT);
}

void loop() {
  sensor_value = analogRead(pot_pin);
  sensor_value = map(sensor_value, 0, 1023, 0, 255);  // Map value 0-1023 to 0-255 (PWM)
  Serial.println(sensor_value);                       // Send PWM value to led
  analogWrite(led_pin, sensor_value);
  delay(100);
}
 
