/*  
 *   TODO: build python server on Mac that listerns to POST requests from ESP32's IP address on port 80 
 *   and then writes to file on the Mac file system
  ESP32 RHT03 and Phant Example
  This program uses the RHT03 (DHT22) to measure temperature and humidity, then posts these data to phant.io
  It has not been tested with any other sensor, e,g., it will not work with a DHT11
  John K. Bennett
  3/6/2017
  Public Domain, no warranty, etc.
  
  Adapted from RHT03-Example-Serial.cpp, the SparkFun Phant Demo and other code found lying around authored by 
  Jim Lindblom and Shawn Hymel and others.
  I found that the published Phant Arduino library does not appear to work correctly with the ESP32, so this
  example is of the "roll-your-own" variety.

  This example uses the SparkFunRHT03 Ardiuno library:
  https://github.com/sparkfun/SparkFun_RHT03_Arduino_Library
  and the builtin Arduino Wifi library, both as is.
  
  Looking at the front (grated side) of the RHT03, the pinout is as follows:
   1     2        3       4
  VCC  DATA  No-Connect  GND
  
  Connect the data pin to an ESP32 bi-diretional pin (I used pin 4 below). Power the RHT03 with 3.3V from the ESP32.
  Reportedly, some DHT22's have their pins 3 and pin 4 swapped. Both can be safely grounded, if this is a concern.
  
  No pullup resistor is needed on the data pin because the RHTo3 library uses INPUT_PULLUP explicitly.
  
  Development environment specifics:
  Arduino IDE v1.8.1, Feb 2017 pulls of ESP32 Ardunio code and RHT03 library

  To view posted data on your stream, browse to https://data.sparkfun.com/streams/<your-public-key>,
  where "<your-public-key>" is just the public key string that Phant gave you (no quotes, no "<>")

  NOTE: IE11 with recommended security settings appears to not like this URL (fails with 503).  
  For now, use another browser (Firefox works fine).
*/

#include <ArduinoJson.h>
#include <SparkFun_RHT03.h> // for the DHT22 sensor
#include <WiFi.h>           // for the ESP32 radio

#include "Conf.h"

const unsigned int NUM_STREAMS = 1;
const unsigned int WAIT_FOR_RESPONSE_INTERVAL = 10000; // how long we wait for the Phant server to talk to us after GET
const unsigned int POST_RATE = 60 * 1000; // Post timing, one minute
unsigned long previousMillis = 0;
unsigned long currentMillis = 0;

// WiFi Definitions
const char * networkName = (char *) Conf::ENV_NETWORK_NAME;
const char * networkPswd = (char *) Conf::ENV_NETWORK_PSWD;
// Create an instance of the Wifi client
WiFiClient client;

const char * streams[NUM_STREAMS] = {"KJo6Vw3L82tR7QrvJz3L"};
  
// Pin Definitions
const int RHT03_DATA_PIN = 4; // RHT03 data pin. This can be any GPIO bi-directional pin

// Internet port to use:
const int port = 80; // http port number

// RHT03 Object Creation
RHT03 rht; // This creates a RTH03 object, which we'll use to interact with the sensor

// Phant Stream Information - change this to match your stream definition
const char * phantHost = "data.sparkfun.com";
const char * publicKey = (char *) Conf::ENV_PHANT_ID;
const char * privateKey = (char *) Conf::ENV_PHANT_SECRET;

const char * googleHost = "sheets.googleapis.com";
const char * googleSheetId = "1g6jsHMPBsE0Qg7h3zShSzP2kuHGwIQYG8CPTnlmEhqs";

// **** These are the fields in this example; change them to match the Phant stream that you defined ****
const byte NUM_FIELDS = 4; // the total number of streams in your Phant Stream
const String fieldNames[NUM_FIELDS] = {"humidity", "location", "tempc", "tempf"}; // the field names in your stream

String fieldData[NUM_FIELDS]; // where we will put the data to send

String response;

struct point {
  String location;
  String humidity;
  String tempc;
  String tempf;
};

void setup() {
  Serial.begin(115200); // Serial is used to print sensor readings.
  
  // Connect to the WiFi network (see function below loop)
  connectToWiFi(networkName, networkPswd);  // let's get connected
  
  // Call rht.begin() to initialize the sensor and our data pin
  rht.begin(RHT03_DATA_PIN);

//  getDataFromPhant();
//  postDataToGoogle();
}

void loop() {
  // Call rht.update() to get new humidity and temperature values from the sensor.
  int updateRht = rht.update();

  
  
  // If successful, the update() function will return 1.
  // If update fails, it will return a value <0
  if (updateRht == 1) {
    Serial.println("Sensor update succeeded.");
    // The humidity(), tempC(), and tempF() functions can be called -- after 
    // a successful update() -- to get the last humidity and temperature
    // value 
    float humidity = rht.humidity(); // these instance variable names are defined in the RHT library
    float tempc = rht.tempC();
    float tempf = rht.tempF();

    Serial.println("Humidity: " + (String) humidity + ", temperature C: " + (String) tempc + ", temperature F: " + (String) tempf);

    // Prepare our data:
    fieldData[0] = String(humidity);
    fieldData[1] = String("West%20Washington%20Park");
    fieldData[2] = String(tempc);
    fieldData[3] = String(tempf);
    
    if (WiFi.status() != WL_CONNECTED) {
      Serial.println("Lost Connection - No Data Sent"); //:(
    } else { 
      // We still have a connection; send data to Phant
      Serial.println("Posting data to Phant");
      postDataToPhant(); // the postDataToPhant() function does all the work; check it out below.
      
    }
  } else { // failed to read DHT sensor; wait a bit
    // If the update failed, try delaying for RHT_READ_INTERVAL_MS ms before
    // trying again.
    Serial.println("Sensor update failed");
    delay(RHT_READ_INTERVAL_MS);
  } 
  delay(POST_RATE); // Wait before posting again
}

void connectToWiFi(const char * ssid, const char * pwd) {
  Serial.println("Connecting to WiFi network: " + String(ssid));

  WiFi.begin(ssid, pwd);
  delay(10000); // It takes a while to negotiate a connection so wait 10 secs

  if (WiFi.status() != WL_CONNECTED) {
    Serial.println("Did Not Connect"); //:(
  } else {
    Serial.println();
    Serial.println("WiFi connected!");
    Serial.print("IP address: ");
    Serial.println(WiFi.localIP());
    Serial.println();
  }
}

void postDataToPhant() { 
  Serial.println("Establishing TCP/IP connection...");
  if (client.connect(phantHost, port)) { 
    Serial.println("TCP/IP Connection Established!");
    // Post the data!
    // Using GET vs POST to avoid side effects that make Phant unhappy
    // First write the preamble stuff
    Serial.println("Public key: " + (String) publicKey + ", private key: " + (String) privateKey);
    
    client.print("GET /input/");
    client.print(publicKey);
    client.print("?private_key=");
    client.print(privateKey);
    
    // Now enumerate through our data fields; these MATCH the stream definition!
    for (int i = 0; i < NUM_FIELDS; ++i) { 
      client.print("&"); 
      client.print(fieldNames[i]);
      client.print("="); 
      client.print(fieldData[i]);
    }
    
    // Now write the suffix stuff
    client.println(" HTTP/1.1\r\n");
    client.print("Host: ");
    client.println(phantHost); 
    client.println("\r\nConnection: close\r\n\r\n"); 
    client.println(); 
  } else {
    Serial.println(F("Connection failed"));
  } 
  
  // We're not done; we now need to listen to the port to see if things went OK
  // Check for a response from the server, and route it to the serial port a character at a time.
  // We don't want to wait forever with the connection hung open, so timeout after a while
  
  previousMillis = currentMillis = millis();
  while (client.connected() && ((currentMillis - previousMillis) < WAIT_FOR_RESPONSE_INTERVAL)) {
    if (client.available()) {
      char c = client.read();
      Serial.print(c);
    }
    currentMillis = millis();      
  }
  Serial.println();
  Serial.println("Data Sent (but not necessarily successfully - check response!)");
  Serial.println("Stopping TCP/IP client");
  client.stop();
}

void getDataFromPhant() {
  const unsigned int offset = 0;
  const unsigned int limit = 100;
  
  for (int i = 0; i < NUM_STREAMS; ++i) {
    Serial.println("Getting data for stream: " + String(streams[i]));
    Serial.println("Establishing TCP/IP connection...");
    if (client.connect(phantHost, port)) { 
      Serial.println("TCP/IP Connection Established!");

      client.print("GET /output/");
      client.print(String(streams[i]));
      client.print(".csv?offset=");
      client.print(String(offset));
      client.print("&limit=");
      client.print(String(limit));
      
      // Now write the suffix stuff
      client.println(" HTTP/1.1\r\n");
      client.print("Host: ");
      client.println(phantHost); 
      client.println("\r\nConnection: close\r\n\r\n"); 
      client.println(); 
    } else {
      Serial.println(F("Connection failed"));
    } 
    
    previousMillis = currentMillis = millis();
    response = "";
    while (client.connected() && ((currentMillis - previousMillis) < WAIT_FOR_RESPONSE_INTERVAL)) {
      if (client.available()) {
        char c = client.read();
        Serial.print(c);
//        response = String(response + c);
//        response += String(c);
      }
      currentMillis = millis();
    }
    Serial.println("Response: ");
    Serial.println(response);
    Serial.println("Stopping TCP/IP client");
    client.stop();
  }

}


void postDataToGoogle() {
  Serial.println("Posting data to Google Sheets...");
  Serial.println("Establishing TCP/IP connection...");
  if (client.connect(googleHost, port)) { 
    Serial.println("TCP/IP Connection Established!");

    client.print("POST /v4/spreadsheets/");
    client.print(googleSheetId);
    client.print("/values:batchUpdate");

//{
//    "valueInputOption": "value_input_option",
//    "data": [
//        {
//            "range": "range",
//            "values" : [
//                [
//                    // Cell values ...
//                ],
//                // Additional rows ...
//            ]
//        },
//        // Additional ranges to update ...
//    ]
//}

    // Now write the suffix stuff
    client.println(" HTTPS/1.1\r\n");
    client.print("Host: ");
    client.println(googleHost); 
    client.println("\r\nConnection: close\r\n\r\n"); 
    client.println(); 
  } else {
    Serial.println(F("Connection failed"));
  } 
  
  previousMillis = currentMillis = millis();
  while (client.connected() && ((currentMillis - previousMillis) < WAIT_FOR_RESPONSE_INTERVAL)) {
    if ( client.available() ) {
      char c = client.read();
      Serial.print(c);
    }
    currentMillis = millis();      
  }
  Serial.println();
  Serial.println("Data Sent (but not necessarily successfully - check response!)");
  Serial.println("Stopping TCP/IP client");
  client.stop();
}

