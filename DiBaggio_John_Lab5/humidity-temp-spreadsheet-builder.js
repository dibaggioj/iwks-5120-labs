/**
 * Created by johndibaggio on 3/14/17.
 *
 * GET Phant streams with tag iwks4120 from:
 * https://data.sparkfun.com/streams/tag/iwks4120.json
 *
 * GET data for each Phant stream by public key (e.g., lz6WOD1gKLfmv9a4oRjL) from:
 * https://data.sparkfun.com/output/lz6WOD1gKLfmv9a4oRjL.json
 *
 * Create a spreadsheet with worksheets for humidity, temperature (*C), and temperature (*F) that each contain multiple
 * data series, one for each Phant data stream
 *
 * Need to manually convert sensor reading cell formats from text to number after creating the spreadsheet and then
 * manually create graphs
 *
 * Uses node.js libraries:
 * HTTPS client for making HTTP requests over SSL: https://nodejs.org/api/https.html
 * Library for writing and reading MS Excel 2007+ files: https://www.npmjs.com/package/msexcel-builder
 *
 */

var https = require("https");
var excelbuilder = require("msexcel-builder");

var HOST_PHANT = "data.sparkfun.com";
var TAG_IWKS4120 = "iwks4120";
var DEFAULT_PUBLIC_KEY = "KJo6Vw3L82tR7QrvJz3L";

var allStreamsData = [];
var workbook;
var numCols = 0;
var numRows = 0;
var numSeries = 0;

function processData() {
    workbook = excelbuilder.createWorkbook("./", "humidity-temperature-data.xlsx");

    numSeries = allStreamsData.length;

    var maxPoints = 0;
    for (var i = 0; i < numSeries; i++) {
        if (allStreamsData[i].data.length > maxPoints) {
            maxPoints = allStreamsData[i].data.length;
        }
    }

    numCols = 3 * numSeries;    // timestamp, sensor reading, and location for each
    numRows = 2 + maxPoints;    // max points plus two heading rows

    buildWorksheet("humidity");
    buildWorksheet("tempc");
    buildWorksheet("tempf");

    workbook.createSheet("charts", 10, 100);

    workbook.save(function(ok) {
        if (!ok) {
            workbook.cancel();
        }
    });
}

function buildWorksheet(property) {
    var worksheet = workbook.createSheet(property, numCols, numRows);

    for (var i = 0; i < numSeries; i++) {
        writeSeriesDataToWorksheet(worksheet, property, i);
    }
}

function writeSeriesDataToWorksheet(worksheet, sensorProp, index) {
    var heading;

    switch (sensorProp) {
        case "humidity":
            heading = "% Humidity";
            break;
        case "tempc":
            heading = "Temperature (\u00B0C)";
            break;
        case "tempf":
            heading = "Temperature (\u00B0F)";
            break;
        default:
            heading = "null";
            break;
    }

    var stream = allStreamsData[index];
    var timestampCol = 1 + 3 * index;
    var sensorPropCol = 2 + 3 * index;
    var locationCol = 3 + 3 * index;

    worksheet.set(timestampCol, 1, "Stream");
    worksheet.set(sensorPropCol, 1, stream.publicKey);
    worksheet.set(timestampCol, 2, "Timestamp");
    worksheet.set(sensorPropCol, 2, heading);
    worksheet.set(locationCol, 2, "Location");

    for (var i = 0; i < stream.data.length; i++) {
        var datum = stream.data[i];

        if (("timestamp" in datum) && (sensorProp in datum)) {
            worksheet.set(timestampCol, i + 3, datum.timestamp);
            worksheet.set(sensorPropCol, i + 3, datum[sensorProp])
            if ("location" in datum) {
                worksheet.set(locationCol, i + 3, datum.location)
            }
        }
    }

    worksheet.width(timestampCol, 21);
    worksheet.width(sensorPropCol, 21);
    worksheet.width(locationCol, 21);

    worksheet.font(timestampCol, 1, {"bold": true});
    worksheet.font(sensorPropCol, 1, {"bold": true});
    worksheet.font(timestampCol, 2, {"bold": true});
    worksheet.font(sensorPropCol, 2, {"bold": true});
    worksheet.font(locationCol, 2, {"bold": true});
}

function getAllStreamsData() {
    var req = https.request({
            host: HOST_PHANT,
            path: "/streams/tag/" + TAG_IWKS4120 + ".json",
            method: "GET"
        },
        function(res) {
            var body = [];

            res.on("data", function(data) {
                body.push(data);
            });

            res.on("end", function() {
                var bodyJSON;
                try {
                    bodyJSON = JSON.parse(body.join(""));
                } catch(e) {
                    console.error(e);
                    bodyJSON = {};
                }
                var publicKeys = getStreamsListFromResponse(bodyJSON);
                getStreamData(publicKeys);
            });
        });

    req.end();

    req.on("error", function(e) {
        console.error(e);
    });
}

/**
 * Recursively get data for each stream from list of public keys and add to `allStreamsData` list
 * @param {Array} publicKeys
 */
function getStreamData(publicKeys) {
    var publicKey = publicKeys.pop();
    var req = https.request({
        host: HOST_PHANT,
        path: "/output/" + publicKey + ".json",
        method: "GET"
    },
    function(res) {
        var body = [];

        res.on("data", function(data) {
            body.push(data);
        });

        res.on("end", function() {
            try {
                var bodyJSON = JSON.parse(body.join(""));
                if (Array.isArray(bodyJSON) && bodyJSON.length > 0) {
                    for (var i = 0; i < bodyJSON.length; i++) {
                        lowercaseKeys(bodyJSON[i]);
                    }

                    allStreamsData.push({
                        "publicKey": publicKey,
                        "data": bodyJSON
                    });
                }
            } catch(e) {
                console.error(e);
            }
            if (publicKeys.length > 0) {
                getStreamData(publicKeys);
            } else {
                processData();
            }
        });
    });

    req.end();

    req.on("error", function(e) {
        console.error(e);
    });
}

/**
 * Returns list of stream public keys from /streams/tag/{tagname}.json response
 * @param {Object} body
 * @returns {Array}
 */
function getStreamsListFromResponse(body) {
    var publicKeys = [DEFAULT_PUBLIC_KEY];
    try {
        for (var i = 0; i < body.streams.length; i++) {
            try {
                publicKeys.push(body.streams[i].publicKey);
            } catch (e) {
                console.error(e);
            }
        }
        return publicKeys;
    } catch (e) {
        console.error(e);
        return publicKeys;
    }
}

/**
 * Make object's keys all lowercase
 * @param obj
 */
function lowercaseKeys(obj) {
    var keys = Object.keys(obj);
    var n = keys.length;
    while (n--) {
        var key = keys[n];
        if (key !== key.toLowerCase()) {
            obj[key.toLowerCase()] = obj[key];
            delete obj[key]
        }
    }
}

function main() {
    getAllStreamsData();
}

main();