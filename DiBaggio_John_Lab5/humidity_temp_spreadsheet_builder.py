#!/usr/bin/env
# encoding: utf-8
"""
Created by John DiBaggio on 2017-03-14

GET Phant streams with tag iwks4120 from:
https://data.sparkfun.com/streams/tag/iwks4120.json

GET data for each Phant stream by public key (e.g., lz6WOD1gKLfmv9a4oRjL) from:
https://data.sparkfun.com/output/lz6WOD1gKLfmv9a4oRjL.json

TODO: resolve SSLError error occurring with requests, httplib, and httplib2 libraries...
Note, http://data.sparkfun.com 302 redirects to https://data.sparkfun.com, and these libraries must follow the redirect
since making the http request seems to make no difference

"""
__author__ = 'johndibaggio'

import requests
import httplib
import httplib2

PROTOCOL_PHANT = "https"
HOST_PHANT = "data.sparkfun.com"
TAG_IWKS4120 = "iwks4120"


def get_streams():
    url = PROTOCOL_PHANT + "://" + HOST_PHANT + "/streams/tag/" + TAG_IWKS4120 + ".json"
    endpoint = "/streams/tag/" + TAG_IWKS4120 + ".json"
    headers = {
        "Content-Type": "application/json;charset=utf-8",
        "Connection": "close"
    }

    print(url)
    requests.packages.urllib3.disable_warnings()
    response = requests.get(url, headers=headers, verify=False)

    print(requests.api.request('get', url, verify=False))

    print(response.status_code)
    return response

    # c = httplib.HTTPConnection(HOST_PHANT)
    # c.request("GET", endpoint, headers=headers)
    # r = c.getresponse() # Get the server's response and print it
    # print r.status, r.reason
    # data = r.read()
    # print data
    # c.close()

    ## h = httplib2.Http(".cache", ca_certs="/Users/johndibaggio/CU/Dropbox/IWKS_5120/Labs/Lab5/COMODORSACertificationAuthority.crt")
    # h = httplib2.Http(".cache", disable_ssl_certificate_validation=True)
    # resp_headers, content = h.request(url, "GET", headers=headers)
    #
    # print(resp_headers['status'])
    # print(resp_headers['content-type'])


print("Getting streams")
print(get_streams())
