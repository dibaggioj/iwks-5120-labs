/**
 * John DiBaggio
 * 3/7/2017
 * 
 * Get humidity, temperature, location data from known IWKS 4120 streams by public key
 * 
 * Modified from John K. Bennett 3/6/2017
 */

#include <WiFi.h>

#include "Conf.h"

const unsigned int numStreams = 3;                      // number of known Phant streams
const unsigned int intervalWaitForResponse = 10 * 1000; // wait 10 sec for response after GET request to Phant
const unsigned int postRate = 60 * 1000;                // post timing (1 minute)
const int port = 80;                                    // http port number (Internet port number to use)

// WiFi Definitions
const char * networkName = (char *) Conf::ENV_NETWORK_NAME;
const char * networkPswd = (char *) Conf::ENV_NETWORK_PSWD;

const char * streams[numStreams] = {"lz6WOD1gKLfmv9a4oRjL", "NJWm12b0q7Tv0odObVxy", "KJo6Vw3L82tR7QrvJz3L"};
const char * phantHost = "data.sparkfun.com";

unsigned long previousMillis = 0;
unsigned long currentMillis = 0;

WiFiClient client;

void setup() {
  Serial.begin(115200); // Serial is used to print sensor readings.
  
  // Connect to the WiFi network (see function below loop)
  connectToWiFi(networkName, networkPswd);
}

void loop() {
  // Get data from all Phant streams every 10 minutes
  getDataFromPhant();
  delay(10 * 60 * 1000);
}

void connectToWiFi(const char * ssid, const char * pwd) {
  Serial.println("Connecting to WiFi network: " + String(ssid));

  WiFi.begin(ssid, pwd);
  delay(10000); // It takes a while to negotiate a connection so wait 10 secs

  if (WiFi.status() != WL_CONNECTED) {
    Serial.println("Did Not Connect"); //:(
  } else {
    Serial.println();
    Serial.println("WiFi connected!");
    Serial.print("IP address: ");
    Serial.println(WiFi.localIP());
    Serial.println();
  }
}

void getDataFromPhant() {
  for (int i = 0; i < numStreams; ++i) {
    Serial.println("Getting data for stream: " + String(streams[i]));
    Serial.println("Establishing TCP/IP connection...");
    if (client.connect(phantHost, port)) { 
      Serial.println("TCP/IP Connection Established!");

      client.print("GET /output/");
      client.print(String(streams[i]));
      client.print(".csv");
      
      // Now write the suffix stuff
      client.println(" HTTP/1.1\r\n");
      client.print("Host: ");
      client.println(phantHost); 
      client.println("\r\nConnection: close\r\n\r\n"); 
      client.println(); 
    } else {
      Serial.println(F("Connection failed"));
    } 
    
    previousMillis = currentMillis = millis();
    while (client.connected() && ((currentMillis - previousMillis) < intervalWaitForResponse)) {
      if (client.available()) {
        char c = client.read();
        Serial.print(c);
      }
      currentMillis = millis();
    }
    
    Serial.println("Stopping TCP/IP client");
    client.stop();
  }
}

